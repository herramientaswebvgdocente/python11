from django.contrib import admin
from .models import Paciente

class AdminPaciente(admin.ModelAdmin):
    list_display = ["nombre", "rut", "fecha_ingreso"]
    list_filter = ["rut"]
    search_fields = ["rut"]

    class Meta:
        model = Paciente


# Register your models here.
admin.site.register(Paciente,AdminPaciente)

